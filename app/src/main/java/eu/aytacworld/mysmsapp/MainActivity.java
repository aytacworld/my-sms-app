package eu.aytacworld.mysmsapp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;


public class MainActivity extends ActionBarActivity {

    private EditText numberTextBox;
    private EditText messageTextBox;

    final static int PICK_CONTACT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        numberTextBox =  (EditText)findViewById(R.id.editTextPhoneNo);
        messageTextBox = (EditText)findViewById(R.id.editTextSMS);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onSendClick(View v){
        String nbr = numberTextBox.getText().toString();
        String msg = messageTextBox.getText().toString();

        if (nbr.isEmpty() || msg.isEmpty()) return;

        try {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(nbr, null, msg, null, null);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "SMS failed", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public void onFindContact(View v){
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode){
            case(PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        int index = c.getColumnIndexOrThrow(ContactsContract.Contacts.Data);
                        String[] clms = c.();
                        String res = "";
                        for (String s : clms) {
                            res += s + "\r\n";
                        }
                        messageTextBox.setText(res);

                    }
                    c.close();
                }
                break;
        }
    }
}